// routes - contains all the endpoints for the application; instead of putting a lot of routes in the index.js, we separate them in other file, routes.js (databaseRoutes.js i.e. taskRoutes.js)

const express = require("express")

//Creates a router instanc that functions as a middleware and routing system; allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

//allows us to use the controllers' functions
const taskController = require("../controllers/controllers.js")

//before, "/" means accessing localhost:3000/
//through the use of app.use("/tasks",routes)in index.js for the routes.js,

//accessing different schema, assuming we have others, would set up another app.use in the index.js and other files inside models, routes and controllers folders
// for example, if you add another schema that is Users, the base uri for that would be set by app.use("/users",routes) and accessing its"/"url inside routes would be localhost:3000/users/
// route to get all tasks
router.get("/", (req,res)=>{
	//taskController - a controller file that is existing in the repo
	//getAllTasks is a function that is encoded inside the taskController file
	//use.then to wait or the getAllTasks function to be executed before proceeding  to the statements (res.send(result))
	taskController.getAllTasks().then(result => res.send(result));
})

//route for creating a task
router.post("/",(req,res)=>{
	taskController.createTask(req.body).then(result => res.send(result));
})

/*
	:- wildcard; if tagged with a parameter, the app will automatically look for the parameter that is in the url
*/
//localhost:3000/tasks/6267e0fb0e07b5836bcb1a0a
router.delete("/:id", (req,res)=>{
	//URL parameter values are accessed through the request object's "params" property; the specified parameter/property of the object will match the URL parameter endpoint
	taskController.deleteTask(req.params.id).then(result=> res.send(result))
})

router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

//ACTIVITY 31
//1.
router.get("/:id", (req,res)=>{
	taskController.getTask(req.params.id).then(result=> res.send(result))
})

//module.exports- a way for js to treat the value/file as a package that can be imported/be used by other files
module.exports=router

//2.
router.get("/:id", (req,res)=>{
	taskController.updateStatus(req.params.id).then(result=> res.send(result))
})
