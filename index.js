/*
Scheme is what
routes is how
controllers is why
*/
const express = require("express");

const mongoose = require("mongoose");

//load the package that is inside the routes.js file in the repo
const taskRoute= require("./routes/routes.js");

const app = express();

const port = 3000;

app.use(express.json()); 
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://eltonUser:chicharon@cluster0.7jgiw.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})
let db=mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open",() => console.log("We're connected to the database"))

//gives the app an access to the routes needed
app.use("/tasks",taskRoute)








app.listen(port,() => console.log(`Now listening to port ${port}`));